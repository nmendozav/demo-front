import React from 'react';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import { green } from '@material-ui/core/colors';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


const axios = require('axios');
axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#282c34',
  },
  paper: {
    padding: theme.spacing(4),
    margin: 'auto',
    maxWidth: 600,
  },
  paperIn: {
    margin: 'auto',
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Main() {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [persona, setPersona] = React.useState({rut:'', nombre:'', apellido:'', fechaNacimiento:''});
  const [personaResult, setPersonaResult] = React.useState({});
  const [rut, setRut] = React.useState('');
  const [rutNew, setRutNew] = React.useState('');

  const [openSnack, setOpenSnack] = React.useState(false);
  const [openErr, setOpenErr] = React.useState(false);
  
  const onChangeRutPersona = (e)=>{
    var regEx = /[Kk0-9_-]+$/g;
    if(regEx.test(e.target.value))
      setRutNew(formateaRut(e.target.value));
    if(e.target.value === '-')
      setRutNew('');
  }

  const onChangeNombrePersona = (e)=>{
    let per = persona;
    per.nombre = e.target.value;
    console.log(per);
    setPersona(per);
  }
  
  const onChangeApellidoPersona = (e)=>{
    let per = persona;
    per.apellido = e.target.value;
    setPersona(per);
  }

  const onChangeFechaNacPersona = (e)=>{
    let per = persona;
    per.fechaNacimiento = e.target.value;
    setPersona(per);
  }

  const handleClickOpen = () => { setOpen(true);};
  const handleClose = () => { setPersona({rut:'', nombre:'', apellido:'', fechaNacimiento:''}); setOpen(false);  };

  const handleCrearPersona = () => { 
    let per = persona;
    per.rut = rutNew;
    setPersona(per);
    postPersona(persona)
    .then(res=>{
      setPersona({rut:'', nombre:'', apellido:'', fechaNacimiento:''});
      setOpen(false);
      setOpenSnack(true);
    }).catch(err=>{
      console.log("ERR Persona a agregar err err:", err);
      setOpenErr(true);
    });
  };

  const handleFindPerson = ()=>{
    getPersona(rut).then(res =>{
      console.log(res);
      setPersonaResult(res.data);
    }).catch(err =>{
      setPersonaResult({nombre:'-'});
    });
  }

  const onChangeRut = (e) => {
    var regEx = /[Kk0-9_-]+$/g;
    if(regEx.test(e.target.value))
      setRut(formateaRut(e.target.value));
    if(e.target.value === '-')
      setRut('');
  }

  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });

  return (
    <div className={classes.root} spacing={2}>
      <Paper className={classes.paper}>
        <Grid container spacing={2} className={classes.paperIn}>
          <Grid item xs={4} sm container>
            <Grid item xs container direction="column" spacing={3}>
              <Grid item xs>
                <TextField
                  required
                  id="rut"
                  label="RUT"
                  defaultValue=""
                  variant="outlined"
                  value={rut}
                  onChange={onChangeRut}
                />
              </Grid>
              <Grid item xs>
                <Button variant="contained" fullWidth={true} color="primary" onClick={handleFindPerson}>
                  Buscar
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={8}>
            <Accordion expanded={personaResult.nombre?personaResult.nombre==='-'?false:true:false}>
                <AccordionSummary
                  aria-controls="panel1bh-content"
                  id="panel1bh-header"
                >
                <Typography>DETALLES: {personaResult.nombre?personaResult.nombre==='-'?'Sin resultado':'':'Sin busqueda'}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>NOMBRE: {personaResult.nombre}</Typography>
                <br/>
                <Typography>APELLIDO: {personaResult.apellido}</Typography>
                <br/>
                <Typography>FECHA NACIMIENTO: {personaResult.fechaNacimiento}</Typography>
              </AccordionDetails>
            </Accordion>
          </Grid>
        </Grid>
        <Grid item container direction="row" spacing={2}>  
          <Grid item xs theme={theme}>
            <Button theme={theme} variant="contained" color="primary" fullWidth={true} onClick={handleClickOpen}>
              Agregar Persona
            </Button>
          </Grid>             
        </Grid>
      </Paper>
      <div className={classes.root}>
        
      </div>
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Agregar nueva persona"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Grid container spacing={2}>
                <Grid item xs={5}>
                  <TextField
                    required
                    id="rut"
                    label="RUT"
                    defaultValue=""
                    variant="outlined"
                    fullWidth={true}
                    size="small"
                    value={rutNew}
                    onChange={onChangeRutPersona}
                  />
                </Grid>
                <Grid item xs={5}>
                  <TextField
                    required
                    id="nombre"
                    label="NOMBRE"
                    defaultValue=""
                    variant="outlined"
                    fullWidth={true}
                    size="small"
                    onChange={onChangeNombrePersona}
                  />
                </Grid> 
                <Grid item xs={5}>
                  <TextField
                    required
                    id="apellido"
                    label="APELLIDO"
                    defaultValue=""
                    variant="outlined"
                    fullWidth={true}
                    size="small"
                    onChange={onChangeApellidoPersona}
                  />
                </Grid> 
                <Grid item xs={5}>
                  <TextField
                    required
                    id="fecha"
                    label=""
                    defaultValue="2000-01-01"
                    variant="outlined"
                    fullWidth={true}
                    size="small"
                    value={persona.fechaNacimiento===''?'2000-01-01':persona.fechaNacimiento}
                    type="date"
                    helperText="FECHA NACIMIENTO"
                    onChange={onChangeFechaNacPersona}
                  />
                </Grid> 
              </Grid>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="secondary">
              CANCELAR
            </Button>
            <Button onClick={handleCrearPersona} color="primary" autoFocus>
              AGREGAR
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <div>
        <Snackbar open={openSnack} autoHideDuration={6000} onClose={()=>{setOpenSnack(false)}}>
          <Alert onClose={()=>{setOpenSnack(false)}} severity="success">
          ¡ Registrado con éxito !
          </Alert>
        </Snackbar>
        <Snackbar open={openErr} autoHideDuration={6000} onClose={()=>{setOpenErr(false)}}>
          <Alert onClose={()=>{setOpenErr(false)}} severity="error">
           Error en los datos ingresados
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
}

const formateaRut = (rut)=>{
  // XX.XXX.XXX-X
  const newRut = rut.replace(/\./g,'').replace(/\-/g, '').trim().toLowerCase();
  const lastDigit = newRut.substr(-1, 1);
  const rutDigit = newRut.substr(0, newRut.length-1)
  let format = '';
  for (let i = rutDigit.length; i > 0; i--) {
    const e = rutDigit.charAt(i-1);
    format = e.concat(format);
    if (i % 3 === 0){
      format = '.'.concat(format);
    }
  }
  return format.concat('-').concat(lastDigit);
}

const getPersona = (rut) =>{
  return new Promise((resolve, reject)=>{
    axios.get(`/person?rut=${rut.replace('.','').replace('.','')}`)
    .then(function (response) {
      // handle success
      console.log(response);
      resolve(response)
    })
    .catch(function (error) {
      // handle error
      console.log(error);
      reject(error)
    });
  });
}

const postPersona = (persona) =>{
  return new Promise((resolve, reject)=>{
    if(persona.rut || persona.nombre || persona.apellido || persona.fechaNacimiento ){
      persona.rut = persona.rut.replace('.','').replace('.','');
      console.log("Persona a agregar:", persona);
      axios.post(`/createPerson`,persona)
      .then(function (response) {
        // handle success
        console.log(response);
        resolve(response)
      })
      .catch(function (error) {
        // handle error
        console.log(error);
        reject(error);
      });
    }else{
      console.log("ERR Persona a agregar:", persona);
      reject(persona);
    }
  });
}